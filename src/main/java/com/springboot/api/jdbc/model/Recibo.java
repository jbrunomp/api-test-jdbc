package com.springboot.api.jdbc.model;

public class Recibo {
	
	private Integer idRecibo;
	private String nroRecibo;
	private String montoEmitido;
	private String fgRetencion;
	private Integer idEmpresa;
		
	public Recibo() {
		
	}
	
	public Recibo(Integer id, String nroRecibo, String montoEmitido, String fgRetencion,
			Integer idEmpresa) {
	
		this.idRecibo = id;
		this.nroRecibo = nroRecibo;
		this.montoEmitido = montoEmitido;
		this.fgRetencion = fgRetencion;
		this.idEmpresa = idEmpresa;
		
	}

	public Integer getIdRecibo() {
		return idRecibo;
	}

	public void setIdRecibo(Integer idRecibo) {
		this.idRecibo = idRecibo;
	}

	public String getNroRecibo() {
		return nroRecibo;
	}

	public void setNroRecibo(String nroRecibo) {
		this.nroRecibo = nroRecibo;
	}

	public String getMontoEmitido() {
		return montoEmitido;
	}

	public void setMontoEmitido(String montoEmitido) {
		this.montoEmitido = montoEmitido;
	}

	public String getFgRetencion() {
		return fgRetencion;
	}

	public void setFgRetencion(String fgRetencion) {
		this.fgRetencion = fgRetencion;
	}

	public Integer getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	
	
}
