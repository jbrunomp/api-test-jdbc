package com.springboot.api.jdbc.model;

public class Empresa {
	private Integer idEmpresa;
	private String ruc;
	private String razonSocial;
	private String estadoActual;
public Empresa() {
		
	}
	
	public Empresa(Integer id, String ruc, String razonSocial, String estadoActual) {
	
		this.idEmpresa = id;
		this.ruc = ruc;
		this.razonSocial = razonSocial;
		this.estadoActual = estadoActual;
		
	}
	
	public Integer getIdEmpresa() {
		return idEmpresa;
	}
	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	public String getRuc() {
		return ruc;
	}
	public void setRuc(String ruc) {
		this.ruc = ruc;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public String getEstadoActual() {
		return estadoActual;
	}
	public void setEstadoActual(String estadoActual) {
		this.estadoActual = estadoActual;
	}

}
