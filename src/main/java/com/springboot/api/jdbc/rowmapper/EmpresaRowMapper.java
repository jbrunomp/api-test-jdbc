package com.springboot.api.jdbc.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.springboot.api.jdbc.model.Empresa;

public class EmpresaRowMapper implements RowMapper<Empresa>{

	@Override
	public Empresa mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Empresa empresa = new Empresa();
		
		empresa.setIdEmpresa(rs.getInt("id"));
		empresa.setRuc(rs.getString("ruc"));
		empresa.setRazonSocial(rs.getString("razonSocial"));
		empresa.setEstadoActual(rs.getString("razonSocial"));
		
		
		return empresa;
	}

}
