package com.springboot.api.jdbc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.api.jdbc.model.Empresa;
import com.springboot.api.jdbc.model.Recibo;
import com.springboot.api.jdbc.service.impl.EmpresaServiceImpl;

@RestController
@RequestMapping("/empresa")
public class EmpresaController {
	
	@Autowired
	private EmpresaServiceImpl _empresaService;
	
	@GetMapping(value = "/all", produces = "application/json")	
	public List<Empresa> getAllPersonas(){
		return _empresaService.getAllEmpresas();
	}
	
	@GetMapping(value = "/get/{id}", produces = "application/json")	
	public Empresa getPersona(@PathVariable ("id") Integer id){
		return _empresaService.getEmpresa(id);
	}
	
	@PostMapping(value = "/save", produces = "application/json")	
	public List<Empresa> saveEmpresa(@RequestBody Empresa persona){
		
		_empresaService.saveEmpresa(persona);
		
		return _empresaService.getAllEmpresas();
	}	
	
	@PostMapping(value = "/save", produces = "application/json")	
	public List<Empresa> saveRecibo(@RequestBody Recibo recibo){
		
		_empresaService.saveRecibo(recibo);
		
		return _empresaService.getAllEmpresas();
	}	
	

}
