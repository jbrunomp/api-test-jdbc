package com.springboot.api.jdbc.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.springboot.api.jdbc.dao.EmpresaDao;
import com.springboot.api.jdbc.model.Empresa;
import com.springboot.api.jdbc.model.Recibo;
import com.springboot.api.jdbc.rowmapper.EmpresaRowMapper;

@Repository
public class EmpresaDaoImpl extends JdbcDaoSupport implements EmpresaDao {

	public EmpresaDaoImpl(DataSource dataSource) {
		this.setDataSource(dataSource);
	}
	
	public List<Empresa> getAllEmpresas() {
		
		List<Empresa> listaEmpresas = new ArrayList<Empresa>();
		
		String sql = " SELECT id, ruc, razonSocial, estadoActual\n" + 
				" FROM microservicios.empresa";
		
		try {
			
			RowMapper<Empresa> empresaRow = new EmpresaRowMapper();
			listaEmpresas = getJdbcTemplate().query(sql, empresaRow);
			logger.debug("Se han listado "+listaEmpresas.size()+" personas");
					
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return listaEmpresas;
	}

	
	public Empresa getEmpresa(Integer id) {
		Empresa empresa = new Empresa();	
		List<Empresa> listaEmpresas = new ArrayList<Empresa>();
		
		String sql = " SELECT id, ruc, razonSocial, estadoActual\n" + 
				" FROM microservicios.empresa where id='"+id+"'";
				
		try {
			
			RowMapper<Empresa> empresaRow = new EmpresaRowMapper();
			listaEmpresas = getJdbcTemplate().query(sql, empresaRow);
			
			empresa = listaEmpresas.get(0);
			
			logger.debug("Se ha traido a la persona "+listaEmpresas.get(0).toString());
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return empresa;
	}

	
	public void saveEmpresa(Empresa empresa) {
		
		String sql = "insert into microservicios.empresa (ruc, razonSocial, estadoActual) "  
				+ "values (?, ?, ?, ?, ?, ?);";
		
		Object[] params = { empresa.getRuc(), empresa.getRazonSocial(), empresa.getEstadoActual()};
		int[] tipos = { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};
		
		try {
			
			int filas = getJdbcTemplate().update(sql, params,tipos);
			
			logger.debug("Se han insertado : "+filas+" filas");
			logger.debug("Se ha registrado a la persona "+empresa.toString());
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

	}
	@Override
	public void saveRecibo(Recibo recibo) {
		
		String sql = "insert into microservicios.recibo (mtoRecibo, montoEmitido, fgRetencion,idEmpresa) "  
				+ "values (?, ?, ?, ?, ?, ?);";
		
		Object[] params = { recibo.getNroRecibo(),recibo.getMontoEmitido(), recibo.getFgRetencion(), recibo.getIdEmpresa()};
		int[] tipos = { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,Types.INTEGER};
		
		try {
			
			int filas = getJdbcTemplate().update(sql, params,tipos);
			
			logger.debug("Se han insertado : "+filas+" filas");
			logger.debug("Se ha registrado al recibo "+recibo.toString());
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

	}

}
