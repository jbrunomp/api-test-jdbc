package com.springboot.api.jdbc.dao;

import java.util.List;

import com.springboot.api.jdbc.model.Empresa;

import com.springboot.api.jdbc.model.Recibo;

public interface EmpresaDao {
	
	List<Empresa> getAllEmpresas();
	Empresa getEmpresa(Integer id);
	void saveEmpresa(Empresa empresa);
	void saveRecibo(Recibo recibo );

}
